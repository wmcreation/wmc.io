<?php namespace App\Http\Controllers;

class HomeController extends Controller {

    /*
    |--------------------------------------------------------------------------
    | Home Controller
    |--------------------------------------------------------------------------
    |
    | This controller renders the "home page" for the application and
    | is configured to only allow guests.
    |
    */

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Show the application home screen to the user.
     *
     * @return Response
     */
    public function index()
    {
        return view('app');
    }

}
