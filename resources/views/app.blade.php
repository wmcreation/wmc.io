<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Home | Weapons of Mass Creation</title>

    <link href="{{ asset('/css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('/css/font-awesome.css') }}" rel="stylesheet">

    <link href="{{ asset('/css/ie.css') }}" rel="stylesheet" type="text/css">

    <!-- Fonts -->
    <link href='https://fonts.googleapis.com/css?family=Roboto:400,300' rel='stylesheet' type='text/css'>

</head>
<body>

<header class="paper-palm brand--secondary">
    <div class="container">
        <div class="u-p">
            <div class="logo">
                <a href="/">
                    <div class="svg-container">
                        @include('svg.wmc-alternative')
                    </div>
                </a>
            </div>
        </div>
    </div>
</header>
<main id="content">
<div>
    <section class="container flex">
        <div class="flex__item u-flex-1/1 u-flex-1/2-desk">
            <article class="u-ph u-pv">
                <blockquote class="chapter--intro u-mb0">At <strong>Weapons of Mass Creation</strong>, we specialise in software development. Our focus is on building quality software, custom made to fit your business needs, with scalability, maintainability and performance in mind. We target various platforms such as the web, mobile and desktop. We use the latest technologies to provide you, your business and your customers with a seamless and unique experience across all platforms.</blockquote>
            </article>
        </div>
        <div class="flex__item u-flex-1/1 u-flex-1/2-desk text--center">
            <div class="u-p">
                <div class="svg-container">
                    @include('svg.device_aspect_ratios')
                </div>
            </div>
        </div>
    </section>
</div>
<div class="brand--secondary">
    <section class="container flex brand--secondary" id="development-practices">
            <div class="[ flex__item u-flex-1/1 u-flex-1/2-desk ]">
                <div class="media u-p">
                    <i class="[ fa fa-comments fa-2x fa-fw ] media__img"></i>
                    <div class="media__body">
                        <h5 class="u-mb0">Discovery</h5>
                        <p>We work closely together with our clients to understand the business needs and model the software according to those needs.</p>
                    </div>
                </div>
            </div>
            <div class="[ flex__item u-flex-1/1 u-flex-1/2-desk ]">
                <div class="media u-p">
                    <i class="[ fa fa-code fa-2x fa-fw ] media__img"></i>
                    <div class="media__body">
                        <h5 class="u-mb0">Implementation</h5>
                        <p>We employ software design principles that enable us to architect and implement quality code.</p>
                    </div>
                </div>
            </div>
            <div class="[ flex__item u-flex-1/1 u-flex-1/2-desk ]">
                <div class="media [ u-p palm-pt0 ]">
                    <i class="[ fa fa-refresh fa-2x fa-fw ] media__img"></i>
                    <div class="media__body">
                        <h5 class="u-mb0">Development process</h5>
                        <p>We use agile and lean software development practices. This enables us to pivot quickly with change.</p>
                    </div>
                </div>
            </div>
            <div class="[ flex__item u-flex-1/1 u-flex-1/2-desk ]">
                <div class="media u-p">
                    <i class="[ fa fa-rocket fa-2x fa-fw ] media__img"></i>
                    <div class="media__body">
                        <h5 class="u-mb0">Testing &amp; Deployment</h5>
                        <p>We use the latest tools and technology to automate software testing. This strategy in turn allows us to safely deploy code continuously.</p>
                    </div>
                </div>
            </div>
    </section>
</div>
<div class="brand--grey">
    <section class="container flex">
        <div class="[ flex__item u-flex-1/1 ] u-p text--center">
            <article>
                <ul class="client-list list-inline">
                    <li class="client-list__item u-m">
                        <a href="http://interprintaruba.com" target="_blank"><img src="images/interprint.svg" alt="Interprint offset printing" title="Interprint offset printing" /></a>
                    </li>
                    <li class="client-list__item u-m">
                        <a href="#"><img src="images/facroes.svg" alt="Fa Croes N.V." title="Fa Croes N.V." /></a>
                    </li>
                    <li class="client-list__item u-m">
                        <a href="http://drukwerkmax.nl" target="_blank"><img src="images/drukwerkmax.svg" alt="DrukwerkMAX" title="DrukwerkMAX" /></a>
                    </li>
                    <li class="client-list__item u-m">
                        <a href="http://rtcnv.nl" target="_blank"><img src="images/rtc.svg" alt="Rotterdam Taxi Centraal" title="Rotterdam Taxi Centraal" /></a>
                    </li>
                    <li class="client-list__item u-m">
                        <a href="http://vrtrade.com" target="_blank"><img src="images/vrtrade.svg" alt="VR Trade industrial rubber products" title="VR Trade industrial rubber products" /></a>
                    </li>
                </ul>

            </article>
        </div>
    </section>
</div>
</main>
<footer>
    <div class="container">
        <div class="u-m">
            <ul class="list-inline">
                <li><li>@include('mailto-icon')</li></li>
                <li><a href="//github.com/moleculezz"><i class="fa fa-github fa-2x fa-fw"></i></a></li>
                <li><a href="//twitter.com/gdarends"><i class="fa fa-twitter fa-2x fa-fw"></i></a></li>
            </ul>
        </div>
        <div class="smallprint u-m">
            <span>Copyright &copy; 2015 - {{ date('Y') }} Weapons of Mass Creation. All rights reserved.</span>
        </div>
    </div>
</footer>

<!-- Scripts
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script> -->
@include('ga')
</body>
</html>
