<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Maintenance | Weapons of Mass Creation</title>

    <link href="{{ asset('/css/maintenance.css') }}" rel="stylesheet">
    <link href="{{ asset('/css/font-awesome.css') }}" rel="stylesheet">

    <!-- Fonts -->
    <link href='//fonts.googleapis.com/css?family=Roboto:400,300' rel='stylesheet' type='text/css'>
</head>
<body class="flextable brand--light-grey">
    <div class="flextable__item">
        <main class="container paper brand--grey u-p">
            <div class="content text--center">
                <div class="logo">
                        @include('svg.wmc')
                </div>
                {{-- <h1 class="title u-m">Be right back.</h1> --}}
            </div>
            <footer class="text--center">
                <div class="u-m">
                    <ul class="list-inline">
                        <li>@include('mailto-icon')</li>
                        <li><a href="//github.com/moleculezz"><i class="fa fa-github fa-2x fa-fw"></i></a></li>
                        <li><a href="//twitter.com/gdarends"><i class="fa fa-twitter fa-2x fa-fw"></i></a></li>
                    </ul>
                </div>
                <div class="smallprint">
                    <span>Copyright &copy; 2015 - {{ date('Y') }} Weapons of Mass Creation. All rights reserved.</span>
                </div>
            </footer>
        </main>
    </div>

    @include('ga')
</body>
</html>
