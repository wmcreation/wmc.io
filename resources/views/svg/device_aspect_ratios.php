<svg version="1.1" id="devices" class="device_aspect_ratios" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 2200 1200" preserveAspectRatio="xMinYMin meet">
<style type="text/css">
<![CDATA[
    .x16-10{fill:#ED217C;}
    .x4-3{fill:#EA3996;}
    .x16-9{fill:#FC449C;}
    .x9-16{fill:#FF66B6;}
    .circle{fill:#FF73C3;}
]]>
</style>
<rect id="x16-10" class="x16-10" y="0.09" width="1920" height="1200"/>
<rect id="x4-3" class="x4-3" x="920" y="240.063" width="1280" height="960"/>
<rect id="x16-9" class="x16-9" x="641.184" y="479.995" width="1280" height="720"/>
<rect id="x9-16" class="x9-16" x="444.403" y="632.36" width="319" height="567.554"/>
<circle id="circle" class="circle" cx="1920.403" cy="1092" r="108"/>
</svg>
